---
layout: layout.html
---

<% set('title', 'Announcing Bitcoin Cash Node v27.1.0') %>
<% set('date', '10 July 2024') %>
<% set('author', 'Bitcoin Cash Node Team') %>

### Release announcement: Bitcoin Cash Node v27.1.0

The Bitcoin Cash Node (BCHN) project is pleased to announce its minor release
version 27.1.0.

This release implements a number of enhancements, bugfixes and performance improvements. It also contains minor modifications to the behavior of some configuration and command line arguments.

BIP70 payment protocol is removed from the node wallet in this release, as it conflicts with full node security requirements. Customers of BIP70 merchants can continue to do so via popular BCH wallets such as Electron-Cash and Bitcoin.com.

Users who are running any of our previous releases are recommended to upgrade to v27.1.0.

For the full release notes, please visit:

[https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v27.1.0](https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v27.1.0)

Executables and source code for supported platforms are available at the
above link, or via the download page on our project website at

[https://bitcoincashnode.org](https://bitcoincashnode.org)


We hope you enjoy our latest release and invite you to join us to improve Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.

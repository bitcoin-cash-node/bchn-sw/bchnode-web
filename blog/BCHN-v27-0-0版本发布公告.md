---
layout: layout.html
---

<% set('title', 'BCHN v27.0.0版本发布公告') %>
<% set('date', '2023年12月13日') %>
<% set('author', 'Bitcoin Cash Node Team') %>

## BCHN v27.0.0版本发布公告

Bitcoin Cash Node (BCHN) 项目在此宣布重大版本更新 27.0.0。

该版本实行了2024年5月15号网络升级规格。

该版本包括区块上限自动适应算法(ABLA):

* CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash
  (2023年11月9日 git hash `ba9ed768` )

以及其他除错和性能提升。

BCHN用户在2024年5月15日前必须升级到此版本，
v25.0.0，v26.x.0及以前的软件将于5月15日到期而失去功能，
并将于4月15日起开始警告用户。

访问以下网址，查看完整版版本说明：

[https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v27.0.0](https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v27.0.0)

该链接中有可执行程序和支持平台的源码，你也可以在BCHN的项目网页进行下载：

[https://bitcoincashnode.org](https://bitcoincashnode.org)

如需了解更多2024年5月15号升级的信息，请访问以下链接：

[https://upgradespecs.bitcoincashnode.org/2024-05-15-upgrade/](https://upgradespecs.bitcoincashnode.org/2024-05-15-upgrade/)

我们希望最新版本能带给你好的体验。让我们一起改善BCH。

BCHN团队

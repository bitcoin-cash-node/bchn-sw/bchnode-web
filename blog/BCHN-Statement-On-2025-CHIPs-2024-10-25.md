---
layout: layout.html
---

<% set('title', 'BCHN maintainers opinion on the VM-Limits and Bigint upgrades for 2025') %>
<% set('date', '25 October 2024') %>
<% set('author', 'Bitcoin Cash Node Team') %>

## BCHN maintainers' opinion on the VM-Limits and Bigint upgrades for 2025

Two Cash Improvement Proposals (CHIPs) are currently frontrunners for lock in on 15th November 2024, and activation on 15th May 2025: [CHIP-2021-05 VM Limits: Targeted Virtual Machine Limits](https://github.com/bitjson/bch-vm-limits/) and [CHIP-2024-07 BigInt: High-Precision Arithmetic for Bitcoin Cash](https://github.com/bitjson/bch-bigint). Despite CHIP-2024-07-BigInt's late introduction, both of them have matured as of October 2024, and are receiving widespread support from the community.

The Bitcoin Cash Node team was intimately involved with implementing and testing both proposals. **The maintainers can now confidently say that these CHIPs seem to be in sufficiently good shape, and worthy of our endorsement.**

We have additionally erected a temporary server at https://index.imaginary.cash with Fulcrum port at 50006, which will serve as a testing ground for post-activation contracts until the proposals' likely activation on Chipnet this November. Anyone interested in preliminary tests is welcome to use this facility as well.

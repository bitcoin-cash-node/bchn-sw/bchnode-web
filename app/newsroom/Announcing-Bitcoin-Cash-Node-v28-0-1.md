---
layout: layout.html
---

<% set('title', 'Announcing Bitcoin Cash Node v28.0.1') %>
<% set('date', '30 December 2024') %>
<% set('author', 'Bitcoin Cash Node Team') %>

### Release announcement: Bitcoin Cash Node v28.0.1

The Bitcoin Cash Node (BCHN) project is pleased to announce its minor release
version 28.0.1.

Just like v28.0.0 before it, this release includes the May 15, 2025 network upgrade.

It includes various corrections and improvements, most notably a performance 
improvement for large block processing, which should benefit non-mining and mining nodes alike.

Before May 15, 2025, BCHN users must update to v28.0.0, v28.0.1 or newer.
The v27.x.0 software will expire on May 15, 2025, and will start
to warn of the need to update ahead of time, from April 15, 2025 onward.

For the full release notes, please visit:

  https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v28.0.1

Executables and source code for supported platforms are available at the
above link, or via the download page on our project website at

  https://bitcoincashnode.org

For more information about the May 15, 2025 network upgrade, visit

  https://upgradespecs.bitcoincashnode.org/2025-05-15-upgrade/

We hope you enjoy our latest release and invite you to join us to improve
Bitcoin Cash.

Sincerely,

The Bitcoin Cash Node team.

---
layout: layout.html
---

<% set('title', 'BCHN v28.0.1版本发布公告') %>
<% set('date', '2024年12月30日') %>
<% set('author', 'Bitcoin Cash Node Team') %>

# BCHN v28.0.1版本发布公告

Bitcoin Cash Node (BCHN) 项目在此宣布小幅版本更新 28.0.1。

如同之前的v28.0.0一样，此版本实行了2025年5月15号网络升级规格。

此版本也包含了一些修正和性能提升，其中对于大区块处理的效能改进尤为显著，矿池及非矿池用户皆可受益。

BCHN用户在2025年5月15日前必须升级到v28.0.0，v28.0.1或更新版本，
v27.x.0及以前的软件将于5月15日到期而失去功能，
并将于4月15日起开始警告用户。

访问以下网址，查看完整版版本说明：

  https://github.com/bitcoin-cash-node/bitcoin-cash-node/releases/tag/v28.0.1

该链接中有可执行程序和支持平台的源码，你也可以在BCHN的项目网页进行下载：

  https://bitcoincashnode.org

如需了解更多2025年5月15号升级的信息，请访问以下链接：

  https://upgradespecs.bitcoincashnode.org/2025-05-15-upgrade/

我们希望最新版本能带给你好的体验。让我们一起改善BCH。

BCHN团队
